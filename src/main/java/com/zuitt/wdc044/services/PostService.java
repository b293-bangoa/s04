package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.stereotype.Service;

public interface PostService {

    void createPost(String stringToken, Post post);

    Iterable<Post> getPosts();

}
